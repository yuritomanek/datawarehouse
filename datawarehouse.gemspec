# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'datawarehouse/version'

Gem::Specification.new do |spec|
  spec.name          = "datawarehouse"
  spec.version       = Datawarehouse::VERSION
  spec.authors       = ["Yuri Tomanek"]
  spec.email         = ["yuri@tomanek.com.au"]
  spec.description   = %q{Datawarehouse is a simple wrapper for the ATDW service.}
  spec.summary       = %q{Datawarehouse a simple wrapper}
  spec.homepage      = "http://yuritomanek.github.io/datawarehouse/"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "httparty", "0.13.5"
  spec.add_dependency "friendly_id", "4.0.10.1"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
