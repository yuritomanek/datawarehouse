# Datawarehouse

**Datawarehouse** is a simple wrapper for the [ATDW](http://www.atdw.com.au/) service.

[![Code Climate](https://codeclimate.com/github/yuritomanek/datawarehouse.png)](https://codeclimate.com/github/yuritomanek/datawarehouse)
[![Dependency Status](https://gemnasium.com/yuritomanek/datawarehouse.png)](https://gemnasium.com/yuritomanek/datawarehouse)
