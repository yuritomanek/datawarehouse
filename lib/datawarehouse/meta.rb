module Datawarehouse
  class Meta < ActiveRecord::Base

    self.table_name = 'datawarehouse_meta'

    attr_accessible :meta_type, :name

    belongs_to :metable, :polymorphic => true

    scope :facilities, where(:meta_type => "Entity Facility")
    scope :room, where(:meta_type => "Service Facility")
    scope :experiences, where(:meta_type => "Experience")
    scope :accreditation, where(:meta_type => "Accreditation")

  end
end
