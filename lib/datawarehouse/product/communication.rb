module Datawarehouse
  class Product
    class Communication < OpenStruct

      def tollfree?
        attribute_id_communication_description == "Tollfree Enquiries"
      end

      def telephone?
        attribute_id_communication_description == "Telephone Enquiries"
      end

      def mobile?
        attribute_id_communication_description == "Mobile Telephone Enquiries"
      end

      def fax?
        attribute_id_communication_description == "Facsimile Enquiries"
      end

      def email?
        attribute_id_communication_description == "Email Enquiries"
      end

      def url?
        attribute_id_communication_description == "URL Enquiries"
      end

    end
  end
end
