module Datawarehouse
  class Product
    class Multimedia < OpenStruct

      def image?
        attribute_id_multimedia_content == "IMAGE"
      end

    end
  end
end
