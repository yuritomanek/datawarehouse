module Datawarehouse
  class TourService < ActiveRecord::Base
    include Datawarehouse::Helper

    extend FriendlyId
    friendly_id :name, use: :slugged

    self.table_name = 'datawarehouse_tour_services'

    has_many :images, :as => :imageable, :class_name => "::Datawarehouse::Image", :dependent => :destroy
    has_many :metas, :as => :metable, :class_name => "::Datawarehouse::Meta", :dependent => :destroy
    has_many :classifications, :as => :classificationable, :class_name => "::Datawarehouse::Classification", :dependent => :destroy
    has_many :external_system_codes, :as => :externalable, :class_name => "::Datawarehouse::ExternalSystemCode", :dependent => :destroy
    has_many :awarded_items, as: :awardable
    has_many :award_items, through: :awarded_items

    def self.import_product(tour, service)

      tour_service = TourService.where(:tour_id => tour.id).first

      if tour_service.blank?
        tour_service = TourService.new
      end

      tour_service.tour_id = tour.id
      tour_service.service_id = service.service_id
      tour_service.product_id = service.product_id
      tour_service.import_basic_details(service)
      tour_service.save

      # tour.import_address_details(product)
      # tour.import_communication_details(product)
      # tour.import_images(product)
      # tour.import_meta(product)
      # tour.import_regions(product)
      # tour.import_classifications(product)
      # tour.import_external_system_codes(product)

      return tour_service
    end

    def import_basic_details(service)
      service_record = service.service_record
      self.name = service_record.service_name
      self.description = service_record.service_description
      self.duration = service_record.tour_duration
      self.duration_unit = service_record.attribute_id_time_unit
      self.pickup_available = service_record.pickup_available_flag if service_record.pickup_available_flag
      self.pickup_available_text = service_record.pickup_available_text
      self.minimum_capacity = service_record.minimum_capacity
      self.maximum_capacity = service_record.maximum_capacity
      self.pet_friendly = service_record.pets_allowed_flag if service_record.pets_allowed_flag
      self.children_friendly = service_record.children_catered_for_flag if service_record.children_catered_for_flag
      self.disabled_friendly = service_record.disabled_access_flag if service_record.disabled_access_flag
    end

  end
end
