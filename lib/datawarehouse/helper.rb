module Datawarehouse
  module Helper

    def build_method(hash, hash_string, class_string)
      begin
        if hash[hash_string]
          object = Object.const_get("#{class_string}")
          if hash[hash_string]["row"].class == Array
            array = []
            hash[hash_string]["row"].each do |hash|
              array << object.new(hash)
            end
            return array
          else
            array = []
            array << object.new(hash[hash_string]["row"])
            return array
          end
        else
          return nil
        end
      rescue
        return nil
      end
    end

    def import_address_details(product)
      product_address = product.product_address[0]
      self.address_line_1 = product_address.address_line_1
      self.address_line_2 = product_address.address_line_2
      self.address_line_3 = product_address.address_line_3
      self.suburb = product_address.city_name
      self.state = product_address.state_name
      self.postcode = product_address.address_postal_code
      self.lat = product_address.geocode_gda_latitude
      self.lng = product_address.geocode_gda_longitude
      self.save
    end

    def import_communication_details(product)
      product_communication = product.product_communication
      if product_communication
        if product_communication.class == Array
          product_communication.each do |communication|
            build_communication(self, communication)
          end
          self.save
        else
          build_communication(self, product_communication)
          self.save
        end
      end
    end

    def build_communication(object, communication)
      if communication.tollfree?
        object.toll_free_phone = communication.communication_detail
      elsif communication.telephone?
        object.phone = communication.communication_detail
      elsif communication.mobile?
        object.mobile = communication.communication_detail
      elsif communication.fax?
        object.fax = communication.communication_detail
      elsif communication.email?
        object.email = communication.communication_detail
      elsif communication.url?
        object.website = communication.communication_detail
      end
    end

    def import_images(product)
      self.images.destroy_all
      multimedia = product.product_multimedia
      if multimedia
        if multimedia.class == Array
          multimedia.each do |media|
            if media.image?
              build_images(self, media)
            end
          end
        else
          if multimedia.image?
            build_images(self, multimedia)
          end
        end
      end
    end

    def build_images(object, media)
      image = object.images.new
      image.url = media.server_path
      image.orientation = media.attribute_id_size_orientation
      image.alt_text = media.alt_text
      image.image_id = media.multimedia_id
      image.width = media.width
      image.height = media.height
      image.save
    end

    def import_meta(product)
      self.metas.destroy_all
      product_attributes = product.product_attribute
      if product_attributes
        if product_attributes.class == Array
          product_attributes.each do |product_attribute|
            build_meta(self, product_attribute)
          end
        else
          build_meta(self, product_attributes)
        end
      end
    end

    def build_meta(object, product_attribute)
      meta = object.metas.new
      meta.meta_type = product_attribute.attribute_type_id_description
      meta.name = product_attribute.attribute_id_description
      meta.save
    end

    def import_regions(product)
      self.regions.destroy_all
      import_area_regions(product)
      import_domestic_regions(product)
    end

    def reimport_regions
      client = Datawarehouse::Client.new(API_KEY)
      product = client.get_product(self.product_id)
      self.import_regions(product) if product
    end

    def import_area_regions(product)
      product.product_address.each do |product_address|
        product_address = product_address.product_address_area_relationship
        if product_address
          if product_address.class == Array
            product_address.each do |paar|
              build_region(self, paar)
            end
          else
            build_region(self, product_address)
          end
        end
      end
    end

    def import_domestic_regions(product)
      product.product_address.each do |product_address|
        product_address = product_address.product_address_domestic_region_relationship
        if product_address
          if product_address.class == Array
            product_address.each do |paar|
              build_domestic_region(self, paar)
            end
          else
            build_domestic_region(self, product_address)
          end
        end
      end
    end

    def build_region(object, product_address)
      region = object.regions.new
      region.region_name = product_address["row"]["area_name"].gsub("/"," ")
      region.region_type = product_address["row"]["area_type"]
      region.save
    end

    def build_domestic_region(object, product_address)
      if product_address["row"].class == Array
        product_address["row"].each do |domestic_region|
          region = object.regions.new
          region.region_name = domestic_region["domestic_region_name"].gsub("/"," ")
          region.region_type = domestic_region["domestic_region_type"]
          region.save
        end
      else
        region = object.regions.new
        region.region_name = product_address["row"]["domestic_region_name"].gsub("/"," ")
        region.region_type = product_address["row"]["domestic_region_type"]
        region.save
      end
    end

    def import_classifications(product)
      self.classifications.destroy_all
      classifications = product.product_vertical_classification
      if classifications
        if classifications.class == Array
          classifications.each do |pvc|
            build_classification(self, pvc)
          end
        else
          build_classification(self, classifications)
        end
      end
    end

    def build_classification(object, product_vertical_classification)
      classification = object.classifications.new
      classification.type_id = product_vertical_classification.product_type_id
      classification.type_description = product_vertical_classification.product_type_description
      classification.save
    end

    def import_external_system_codes(product)
      self.external_system_codes.destroy_all
      product_external_system_codes = product.product_external_system
      if product_external_system_codes
        if product_external_system_codes.class == Array
          product_external_system_codes.each do |product_external_system_code|
            build_external_system_code(self, product_external_system_code)
          end
        else
          build_external_system_code(self, product_external_system_codes)
        end
      end
    end

    def build_external_system_code(object, product_external_system)
      external_system_code = object.external_system_codes.new
      external_system_code.code = product_external_system.external_system_code
      external_system_code.text = product_external_system.external_system_text
      external_system_code.save
    end

    def disabled?
      disabled_friendly
    end

    def ntap?
      Datawarehouse::Meta.find_by_sql("SELECT id FROM \"datawarehouse_meta\" WHERE metable_id = #{self.id} AND metable_type = '#{self.class}' AND name = 'National Tourism Accreditation Program'").length > 0
    end

    def atap?
      Datawarehouse::Meta.find_by_sql("SELECT id FROM \"datawarehouse_meta\" WHERE metable_id = #{self.id} AND metable_type = '#{self.class}' AND name = 'Australian Tourism Accreditation Program'").length > 0
    end

    def vic?
      Datawarehouse::Classification.find_by_sql("SELECT id FROM \"datawarehouse_classifications\" WHERE classificationable_id = #{self.id} AND classificationable_type = '#{self.class}' AND type_description = 'Visitor Information Centres'").length > 0
    end

    def tqual?
      Datawarehouse::Meta.find_by_sql("SELECT id FROM \"datawarehouse_meta\" WHERE metable_id = #{self.id} AND metable_type = '#{self.class}' AND meta_type = 'TQUAL'").length > 0
    end

    def eco?
      Datawarehouse::Meta.find_by_sql("SELECT id FROM \"datawarehouse_meta\" WHERE metable_id = #{self.id} AND metable_type = '#{self.class}' AND meta_type = 'Accreditation' AND name= 'National Ecotourism Accreditation Program'").length > 0
    end

    def roc?
      Datawarehouse::Meta.find_by_sql("SELECT id FROM \"datawarehouse_meta\" WHERE metable_id = #{self.id} AND metable_type = '#{self.class}' AND meta_type = 'Accreditation' AND name= 'Respecting Our Culture Indigenous Accreditation Program'").length > 0
    end

    def climate?
      cac
    end

    def park_wildlife?
      park_wildlife
    end

    def china_ready?
      china_ready
    end

    def camping?
      camping
    end

    def booking_url_supplied?
      return true unless book_url.blank?
      return false
    end

    def twitter
      twitter = external_system_codes.twitter.first
      if twitter
        return twitter.external_system_text
      else
        return false
      end
    end

    def facebook
      facebook = external_system_codes.facebook.first
      if facebook
        return facebook.external_system_text
      else
        return false
      end
    end

    def award_accessor(award, year)
      accessor = "award_#{award}_#{year}"
      singleton_class.class_eval do; attr_accessor "#{accessor}"; end
      send("#{accessor}=", "#{accessor}")
      return "#{accessor}"
    end

    def check_award_item(award_item, year)
      if AwardedItem.where(:awardable_type => self.class, :awardable_id => self.id, :year => year, :award_item_id => award_item.id).first
        return true
      else
        return false
      end
    end

    def txa_shortcodes
      if txa_shortname.blank?
        txas = external_system_codes.where("code = ?", "TXA_MULTI").order("id ASC")
        if txas
          txa_array = []
          txas.each do |txa|
            txa_array << txa.text
          end
          return txa_array
        else
          return nil
        end
      else
        return txa_shortname
      end
    end

    def txa_shortcode
      if txa_shortname.blank?
        txa = external_system_codes.where("code = ?", "TXA_MULTI").order("id ASC").first
        if txa
          return txa.text
        else
          return nil
        end
      else
        return txa_shortname
      end
    end

  end
end
