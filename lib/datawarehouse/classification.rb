module Datawarehouse
  class Classification < ActiveRecord::Base

    self.table_name = 'datawarehouse_classifications'

    attr_accessible :type_id, :type_description

    belongs_to :classificationable, :polymorphic => true

  end
end
