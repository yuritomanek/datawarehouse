module Datawarehouse
  class Service < OpenStruct
    include Datawarehouse::Helper

    attr_accessor :service_record, :service_attribute, :service_departure_date, :service_indicative_adult_rate, :service_multimedia, :service_rate_basis_comment, :service_route, :service_start_location, :service_season_tariff_comment, :service_tariff_adult, :service_tariff_child, :service_tariff_concession, :service_vertical_classification

    def initialize(hash=nil)
      @table = {}
      if hash
        hash.each_pair do |k, v|
          k = k.to_sym
          @table[k] = v
          new_ostruct_member(k)
        end
      end
      build_record(hash)
      build_attribute(hash)
      build_departure_date(hash)
      build_indicative_adult_rate(hash)
      build_multimedia(hash)
      build_rate_basis_comment(hash)
      build_route(hash)
      build_start_location(hash)
      build_season_tariff_comment(hash)
      build_tariff_adult(hash)
      build_tariff_child(hash)
      build_tariff_concession(hash)
      build_vertical_classification(hash)
    end

    def build_record(hash)
      self.service_record = Datawarehouse::Service::Record.new(hash["service_record"])
    end

    def build_attribute(hash)
      self.service_attribute = build_method(hash, "service_attribute", "Datawarehouse::Service::Attribute")
    end

    def build_departure_date(hash)
      self.service_departure_date = build_method(hash, "service_departure_date", "Datawarehouse::Service::DepartureDate")
    end

    def build_indicative_adult_rate(hash)
      self.service_indicative_adult_rate = build_method(hash, "service_indicative_adult_rate", "Datawarehouse::Service::IndicativeAdultRate")
    end

    def build_multimedia(hash)
      self.service_multimedia = build_method(hash, "service_multimedia", "Datawarehouse::Service::Multimedia")
    end

    def build_rate_basis_comment(hash)
      self.service_rate_basis_comment = build_method(hash, "service_rate_basis_comment", "Datawarehouse::Service::RateBasisComment")
    end

    def build_route(hash)
      self.service_route = build_method(hash, "service_route", "Datawarehouse::Service::Route")
    end

    def build_start_location(hash)
      self.service_start_location = build_method(hash, "service_start_location", "Datawarehouse::Service::StartLocation")
    end

    def build_season_tariff_comment(hash)
      self.service_season_tariff_comment = build_method(hash, "service_season_tariff_comment", "Datawarehouse::Service::SeasonTariffComment")
    end

    def build_tariff_adult(hash)
      self.service_tariff_adult = build_method(hash, "service_tariff_adult", "Datawarehouse::Service::TariffAdult")
    end

    def build_tariff_child(hash)
      self.service_tariff_child = build_method(hash, "service_tariff_child", "Datawarehouse::Service::TariffChild")
    end

    def build_tariff_concession(hash)
      self.service_tariff_concession = build_method(hash, "service_tariff_concession", "Datawarehouse::Service::TariffConcession")
    end

    def build_vertical_classification(hash)
      self.service_vertical_classification = build_method(hash, "service_vertical_classification", "Datawarehouse::Service::VerticalClassification")
    end

  end
end
