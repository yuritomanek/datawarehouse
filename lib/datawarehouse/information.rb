module Datawarehouse
  class Information < ActiveRecord::Base
    include Datawarehouse::Helper

    extend FriendlyId
    friendly_id :name, use: :slugged

    self.table_name = 'datawarehouse_informations'

    attr_accessor :preferences, :region_import, :types

    attr_accessible :name, :description, :address_line_1, :address_line_2, :address_line_3, :suburb, :state, :postcode, :lat, :lng, :rate_from, :toll_free_phone, :phone, :mobile, :fax, :email, :website, :pet_friendly, :children_friendly, :disabled_friendly, :book_url, :txa_shortname, :eco_plus, :hidden, :preferences, :region, :types, :atap, :park_wildlife, :china_ready, :x, :y, :alternative_name, :left_right, :camping, :image_id, :active

    has_many :images, :as => :imageable, :class_name => "::Datawarehouse::Image", :dependent => :destroy
    has_many :metas, :as => :metable, :class_name => "::Datawarehouse::Meta", :dependent => :destroy
    has_many :regions, :as => :regionable, :class_name => "::Datawarehouse::Region", :dependent => :destroy
    has_many :classifications, :as => :classificationable, :class_name => "::Datawarehouse::Classification", :dependent => :destroy
    has_many :external_system_codes, :as => :externalable, :class_name => "::Datawarehouse::ExternalSystemCode", :dependent => :destroy
    has_many :awarded_items, as: :awardable
    has_many :award_items, through: :awarded_items
    belongs_to :image , :foreign_key => "image_id" , :class_name => "Gluttonberg::Asset"
    has_many :feedbacks, as: :feedbackable

    def self.import_product(product)

      information = Information.where(:product_id => product.product_id).first

      if information.blank?
        information = Information.new
      end

      information.product_id = product.product_id
      information.import_basic_details(product)
      information.save

      information.import_address_details(product)
      information.import_communication_details(product)
      information.import_images(product)
      information.import_meta(product)
      information.import_regions(product)
      information.import_classifications(product)
      information.import_external_system_codes(product)

      return information
    end

    def import_basic_details(product)
      product_record = product.product_record
      self.name = product_record.product_name
      self.description = product_record.product_description
      self.pet_friendly = product_record.pets_allowed_flag if product_record.pets_allowed_flag
      self.children_friendly = product_record.children_catered_for_flag if product_record.children_catered_for_flag
      self.disabled_friendly = product_record.disabled_access_flag if product_record.disabled_access_flag
      self.rate_from = product_record.rate_from
    end

    def business_type
      "Information"
    end

  end
end
