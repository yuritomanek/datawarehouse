module Datawarehouse
  class Client
    include HTTParty
    base_uri "atlas.atdw.com.au"
    format :xml
    #debug_output $stdout

    def initialize(key)
      @api_key = key
    end

    def search(*params)
      root = do_search(*params)
      return root
    end

    def get_product(product_id)
      response = ""
      options = {:query => {"key" => @api_key, "productId" => product_id, "out" => "xml"}}
      response = self.class.get("/productsearchservice.svc/product", options)
      response = parse_response(response.parsed_response) if response.parsed_response
      if response && response["product_record"]
        product_type = response["product_record"]["product_category_id"]
        return build_product(response, product_type)
      else
        return nil
      end
    end

    def get_product_service(product_id, service_id)
      response = ""
      options = {:query => {"key" => @api_key, "productid" => product_id, "serviceid" => service_id, "out" => "xml"}}
      response = self.class.get("/productsearchservice.svc/productservice", options)
      if response.parsed_response
        return Datawarehouse::Service.new(parse_response(response.parsed_response))
      else
        return nil
      end
    end

    def get_product_hash(product_id)
      response = ""
      options = {:query => {"key" => @api_key, "productId" => product_id, "out" => "xml"}}
      response = self.class.get("/productsearchservice.svc/product", options)
      response = parse_response(response.parsed_response) if response.parsed_response
      return response
    end

    def get_service_hash(product_id, service_id)
      response = ""
      options = {:query => {"key" => @api_key, "productid" => product_id, "serviceid" => service_id, "out" => "xml"}}
      response = self.class.get("/productsearchservice.svc/productservice", options)
      response = parse_response(response.parsed_response) if response.parsed_response
      return response
    end

    def get_product_raw(product_id)
      response = ""
      options = {:query => {"key" => @api_key, "productId" => product_id, "out" => "xml"}}
      response = self.class.get("/productsearchservice.svc/product", options)
      return response
    end

    def bulk_import(category, state, delta = nil)

      product_ids = []
      inactive_product_ids = []
      params = {}

      if delta == true
        delta_date = (Time.now - 2.weeks).strftime("%Y-%m-%d")
        params[:cats] = category
        params[:st] = state
        params[:delta] = delta_date
        results = do_search(params)
      else
        params[:cats] = category
        params[:st] = state
        results = do_search(:cats => category, :st => state)
      end

      results["products"].each do |product|
        if delta == true && product.status != "ACTIVE"
          inactive_product_ids << product.product_id.split("$")[0]
        else
          product_ids << product.product_id.split("$")[0]
        end
      end

      pages = results["number_of_results"].to_i / 20
      pages += 1 if results["number_of_results"].to_i % 20 > 0

      if pages > 1
        (2..pages).each do |p|
          params[:pge] = p
          results = do_search(params)
          results["products"].each do |product|
            if delta == true && product.status != "ACTIVE"
              inactive_product_ids << product.product_id.split("$")[0]
            else
              product_ids << product.product_id.split("$")[0]
            end
          end
        end
      end

      product_ids.each do |product_id|
        product = get_product(product_id)
        if product
          object = Object.const_get("Datawarehouse::#{product_class(product)}")
          object.import_product(product)
        end
      end

      inactive_product_ids.each do |product_id|
        object = Object.const_get("#{product_class_type(category)}")
        product = object.where(:product_id => product_id).first
        if product
          product.hidden = true
          product.active = false
          product.save
        end
      end

      return product_ids
    end

    def check_active(category, state, delta = false)

      product_ids = []
      inactive_product_ids = []
      params = {}

      if delta == true
        delta_date = "2015-03-16"
        params[:cats] = category
        params[:st] = state
        params[:delta] = delta_date
        results = do_search(params)
      else
        params[:cats] = category
        params[:st] = state
        results = do_search(:cats => category, :st => state)
      end

      results["products"].each do |product|
        if delta == true && product.status != "ACTIVE"
          inactive_product_ids << product.product_id.split("$")[0]
        else
          product_ids << product.product_id.split("$")[0]
        end
      end

      pages = results["number_of_results"].to_i / 20
      pages += 1 if results["number_of_results"].to_i % 20 > 0

      if pages > 1
        (2..pages).each do |p|
          params[:pge] = p
          results = do_search(params)
          results["products"].each do |product|
            if delta == true && product.status != "ACTIVE"
              inactive_product_ids << product.product_id.split("$")[0]
            else
              product_ids << product.product_id.split("$")[0]
            end
          end
        end
      end

      inactive_product_ids.each do |product_id|
        object = Object.const_get("#{product_class_type(category)}")
        product = object.where(:product_id => product_id).first
        if product
          product.hidden = true
          product.active = false
          product.save
        end
      end

      return "Active: #{product_ids.count} - Inactive: #{inactive_product_ids.count}"
    end

    private

    def parse_response(response)
      if response["atdw_data_results"]
        if response["atdw_data_results"]["service_distribution"]
          response = response["atdw_data_results"]["service_distribution"]
        else
          response = response["atdw_data_results"]["product_distribution"]
        end
      else
        response = response["atdw_search_response"]["products"]
      end
      return response
    end

    def do_search(*params)
      response = ""
      options = {:query => {"key" => @api_key, "out" => "xml", "size" => "20"}}
      options[:query].merge!(params[0])
      response = self.class.get("/productsearchservice.svc/products", options)

      root = {}
      root["number_of_results"] = response["atdw_search_response"]["number_of_results"]

      if root["number_of_results"].to_i > 0
        response = parse_response(response)
        products = build_search_products(response)
        root["products"] = products
      else
        root["products"] = []
      end
      return root
    end

    def build_search_products(response)
      response = response["product_record"]

      if response.is_a?(Hash)
        (responses_array ||= []) << response
        response = responses_array
      end

      products = []

      response.each do |r|
        product_type = r["product_category_id"]
        product = build_product(r, product_type)
        products << product if product
      end

      return products
    end

    def build_product(response, product_type)
      if product_type == "ACCOMM"
        accommodation = Datawarehouse::Product::Accommodation.new(response)
        product = accommodation
      elsif product_type == "ATTRACTION"
        attraction = Datawarehouse::Product::Attraction.new(response)
        product = attraction
      elsif product_type == "HIRE"
        hire = Datawarehouse::Product::Hire.new(response)
        product = hire
      elsif product_type == "TOUR"
        tour = Datawarehouse::Product::Tour.new(response)
        product = tour
      elsif product_type == "INFO"
        information = Datawarehouse::Product::Information.new(response)
        product = information
      else
        product = nil
      end
    end

    def product_class_type(category)
      if category == "ACCOMM"
        return "Datawarehouse::Accommodation"
      elsif category == "ATTRACTION"
        return "Datawarehouse::Attraction"
      elsif category == "HIRE"
        return "Datawarehouse::Hire"
      elsif category == "TOUR"
        return "Datawarehouse::Tour"
      elsif category == "INFO"
        return "Datawarehouse::Information"
      else
        return nil
      end
    end

    def product_class(product)
      return product.class.to_s.split("::").last
    end

  end
end
