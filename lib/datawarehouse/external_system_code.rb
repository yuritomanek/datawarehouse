module Datawarehouse
  class ExternalSystemCode < ActiveRecord::Base

    self.table_name = 'datawarehouse_external_system_codes'

    belongs_to :externalable, :polymorphic => true

    scope :twitter, where(:code => "Twitter")
    scope :facebook, where(:code => "Facebook")

  end
end
