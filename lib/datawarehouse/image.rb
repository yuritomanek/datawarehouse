module Datawarehouse
  class Image < ActiveRecord::Base

    self.table_name = 'datawarehouse_images'

    belongs_to :imageable, :polymorphic => true
    scope :landscape, where(:orientation => "LANDSCAPE")

  end
end
