module Datawarehouse
  class Hire < ActiveRecord::Base
    include Datawarehouse::Helper

    extend FriendlyId
    friendly_id :name, use: :slugged

    self.table_name = 'datawarehouse_hires'

    attr_accessor :preferences, :region, :types

    attr_accessible :name, :description, :address_line_1, :address_line_2, :address_line_3, :suburb, :state, :postcode, :lat, :lng, :rate_from, :toll_free_phone, :phone, :mobile, :fax, :email, :website, :pet_friendly, :children_friendly, :disabled_friendly, :book_url, :txa_shortname, :eco_plus, :hidden, :preferences, :region, :types, :atap, :park_wildlife, :china_ready, :camping, :image_id, :active, :cac

    has_many :images, :as => :imageable, :class_name => "::Datawarehouse::Image", :dependent => :destroy
    has_many :metas, :as => :metable, :class_name => "::Datawarehouse::Meta", :dependent => :destroy
    has_many :regions, :as => :regionable, :class_name => "::Datawarehouse::Region", :dependent => :destroy
    has_many :classifications, :as => :classificationable, :class_name => "::Datawarehouse::Classification", :dependent => :destroy
    has_many :external_system_codes, :as => :externalable, :class_name => "::Datawarehouse::ExternalSystemCode", :dependent => :destroy
    has_many :awarded_items, as: :awardable
    has_many :award_items, through: :awarded_items
    belongs_to :image , :foreign_key => "image_id" , :class_name => "Gluttonberg::Asset"
    has_many :feedbacks, as: :feedbackable

    def self.import_product(product)

      hire = Hire.where(:product_id => product.product_id).first

      if hire.blank?
        hire = Hire.new
      end

      hire.product_id = product.product_id
      hire.import_basic_details(product)
      hire.save

      hire.import_address_details(product)
      hire.import_communication_details(product)
      hire.import_images(product)
      hire.import_meta(product)
      hire.import_regions(product)
      hire.import_classifications(product)
      hire.import_external_system_codes(product)

      return hire
    end

    def import_basic_details(product)
      product_record = product.product_record
      self.name = product_record.product_name
      self.description = product_record.product_description
      self.pet_friendly = product_record.pets_allowed_flag if product_record.pets_allowed_flag
      self.children_friendly = product_record.children_catered_for_flag if product_record.children_catered_for_flag
      self.disabled_friendly = product_record.disabled_access_flag if product_record.disabled_access_flag
      self.free_entry = product_record.free_entry_flag if product_record.free_entry_flag
    end

    def business_type
      "Hire"
    end

  end
end
