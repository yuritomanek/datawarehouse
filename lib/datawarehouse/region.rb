module Datawarehouse
  class Region < ActiveRecord::Base

    self.table_name = 'datawarehouse_regions'

    attr_accessible :region_type, :region_name

    belongs_to :regionable, :polymorphic => true

  end
end
