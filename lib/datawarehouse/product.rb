module Datawarehouse
  class Product < OpenStruct
    include Datawarehouse::Helper

    attr_accessor :product_record, :product_address, :product_attribute, :product_communication, :product_external_system, :product_multimedia, :product_related_product, :product_vertical_classification, :product_service, :product_comment, :product_name, :product_open_time, :product_season, :product_site

    def initialize(hash=nil)
      @table = {}
      if hash
        hash.each_pair do |k, v|
          k = k.to_sym
          @table[k] = v
          new_ostruct_member(k)
        end
      end
      build_record(hash)
      build_address(hash)
      build_attribute(hash)
      build_communication(hash)
      build_external_system(hash)
      build_multimedia(hash)
      build_related_product(hash)
      build_vertical_classification(hash)
      build_service(hash)
      build_comment(hash)
      build_name(hash)
      build_open_time(hash)
      build_season(hash)
      build_site(hash)
    end

    def build_record(hash)
      self.product_record = Datawarehouse::Product::Record.new(hash["product_record"])
    end

    def build_address(hash)
      self.product_address = build_method(hash, "product_address", "Datawarehouse::Product::Address")
    end

    def build_attribute(hash)
      self.product_attribute = build_method(hash, "product_attribute", "Datawarehouse::Product::Attribute")
    end

    def build_communication(hash)
      self.product_communication = build_method(hash, "product_communication", "Datawarehouse::Product::Communication")
    end

    def build_external_system(hash)
      self.product_external_system = build_method(hash, "product_external_system", "Datawarehouse::Product::ExternalSystem")
    end

    def build_multimedia(hash)
      self.product_multimedia = build_method(hash, "product_multimedia", "Datawarehouse::Product::Multimedia")
    end

    def build_related_product(hash)
      self.product_related_product = build_method(hash, "product_related_product", "Datawarehouse::Product::RelatedProduct")
    end

    def build_vertical_classification(hash)
      self.product_vertical_classification = build_method(hash, "product_vertical_classification", "Datawarehouse::Product::VerticalClassification")
    end

    def build_service(hash)
      self.product_service = build_method(hash, "product_service", "Datawarehouse::Product::Service")
    end

    def build_comment(hash)
      self.product_comment = build_method(hash, "product_comment", "Datawarehouse::Product::Comment")
    end

    def build_name(hash)
      self.product_name = build_method(hash, "product_name", "Datawarehouse::Product::Name")
    end

    def build_open_time(hash)
      self.product_open_time = build_method(hash, "product_open_time", "Datawarehouse::Product::OpenTime")
    end

    def build_season(hash)
      self.product_season = build_method(hash, "product_season", "Datawarehouse::Product::Season")
    end

    def build_site(hash)
      self.product_site = build_method(hash, "product_site", "Datawarehouse::Product::Site")
    end

  end
end
