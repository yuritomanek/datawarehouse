require "httparty"
require "ostruct"
require "friendly_id"
require "thinking-sphinx"

# Alot of files
require_relative "datawarehouse/version"
require_relative "datawarehouse/client"
require_relative "datawarehouse/helper"
require_relative "datawarehouse/product"
require_relative "datawarehouse/product/record"
require_relative "datawarehouse/product/address"
require_relative "datawarehouse/product/attribute"
require_relative "datawarehouse/product/communication"
require_relative "datawarehouse/product/external_system"
require_relative "datawarehouse/product/multimedia"
require_relative "datawarehouse/product/related_product"
require_relative "datawarehouse/product/vertical_classification"
require_relative "datawarehouse/product/service"
require_relative "datawarehouse/product/comment"
require_relative "datawarehouse/product/name"
require_relative "datawarehouse/product/open_time"
require_relative "datawarehouse/product/season"
require_relative "datawarehouse/product/site"
require_relative "datawarehouse/product/accommodation"
require_relative "datawarehouse/product/attraction"
require_relative "datawarehouse/product/hire"
require_relative "datawarehouse/product/tour"
require_relative "datawarehouse/product/information"
require_relative "datawarehouse/service"
require_relative "datawarehouse/service/record"
require_relative "datawarehouse/service/departure_date"
require_relative "datawarehouse/service/indicative_adult_rate"
require_relative "datawarehouse/service/multimedia"
require_relative "datawarehouse/service/rate_basis_comment"
require_relative "datawarehouse/service/route"
require_relative "datawarehouse/service/start_location"
require_relative "datawarehouse/service/season_tariff_comment"
require_relative "datawarehouse/service/tariff_adult"
require_relative "datawarehouse/service/tariff_child"
require_relative "datawarehouse/service/tariff_concession"
require_relative "datawarehouse/service/vertical_classification"
require_relative "datawarehouse/accommodation"
require_relative "datawarehouse/attraction"
require_relative "datawarehouse/hire"
require_relative "datawarehouse/tour"
require_relative "datawarehouse/tour_service"
require_relative "datawarehouse/information"
require_relative "datawarehouse/image"
require_relative "datawarehouse/meta"
require_relative "datawarehouse/region"
require_relative "datawarehouse/classification"
require_relative "datawarehouse/external_system_code"
